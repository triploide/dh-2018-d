<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('productos', function () {
    return 'Hola desde la vista de productos';
});

Route::get('productos/remeras', function () {
    return file_get_contents('remeras.php');
});

Route::post('productos/remeras', function () {
    return 'Action del from';
});

Route::get('categorias/heladeras', function () {
    return '<h1>Heladeras</h1>';
});

/*
Route::get('categorias/{categoria}', function ($categoria) {
    return "<h1>$categoria</h1>";
});
*/

Route::get('categorias/{categoria}/{sub?}', function ($categoria, $sub=null) {
    return "<h1>$categoria $sub</h1>";
});

Route::get('numero/{num1}/{num2?}', function ($num1, $num2=null) {
    $respuesta;
    if ($num2 === null) {
        $respuesta = ($num1 % 2) ? 'Es impar' : 'Es par';
    } else {
        $respuesta = $num1 * $num2;
    }
    return $respuesta;
});

//ejercicios
Route::get('buscar/{peli}', 'EjerciciosController@buscar');


//controllers
Route::get('controllers/vista1', 'EjemploController@vista1');
Route::get('controllers/vista2', 'EjemploController@vista2');


Route::get('peliculas', 'PeliculaController@index');

//Genres
Route::get('genres', 'GenreController@index');
Route::get('genres/last-modified', 'GenreController@lastModified');

//Movies
Route::get('movies', 'MovieController@index');
Route::get('movies/create', 'MovieController@create');
Route::post('movies', 'MovieController@insert');