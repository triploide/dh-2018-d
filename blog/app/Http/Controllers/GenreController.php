<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
    {
    	//$genres = Genre::all(); SELECT * FROM table
    	$genres = Genre::orderBy('name')->get();
    	return view('genres.index', ['generos' => $genres]);
    }

    public function lastModified()
    {
    	$genres = Genre::where('updated_at', '>=', '2018-10-01')->orderBy('name')->get();
    	return view('genres.index', ['generos' => $genres]);
    }
}

//join
//$genres = Genre::join('movies', 'genres.id', '=', 'movies.genre_id');
