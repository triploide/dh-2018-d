<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index()
    {
    	/*
    	$movies = Movie::where('rating', '>=', 8)
    		->where('rating', '<=', 10)
    		->orWhere('release_date', '>=', '2003-01-01')
    		->where('release_date', '<=', '2010-12-31')
    		->get()
    	;
    	*/

    	$movies = Movie::where(function ($query) {
    		$query->where('rating', '>=', 8)
    			->where('rating', '<=', 10)
    		;
    	})->orWhere(function ($query) {
    		$query->where('release_date', '>=', '2003-01-01')
    			->where('release_date', '<=', '2010-12-31')
    		;
    	})->offset(5)->limit(5)->get();

    	return view('movies.index', ['pelis' => $movies]);
    }

    public function create()
    {
        return view('movies.create');
    }

    public function insert()
    {
        dd($_POST);
    }
}
