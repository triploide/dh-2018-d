<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeliculaController extends Controller
{
	public $peliculas = ['Toy Story', 'Avatar'];

    public function index()
    {
    	return view('peliculas/index', ['pelis' => $this->peliculas]);
    }
}
