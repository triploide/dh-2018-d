<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EjerciciosController extends Controller
{
    public function buscar($peli)
    {
    	$peliculas = [
    		1 => 'Titanic',
    		2 => 'Avatar',
    		3 => 'Matrix'
    	];
    	
    	$resultado = 'No hay pelis';
    	foreach ($peliculas as $pelicula) {
    		if ($peli == $pelicula) {
    			$resultado = "Se encontró la peli $pelicula";
    			break;
    		}
    	}
    	return $resultado;
    }
}
