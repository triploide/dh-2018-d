@extends('app')

@section('title', 'Vista 2')

@section('contenido')
    {{-- @parent --}}

    @if ($mostrar)
        <h1>Vista {{ $i }}</h1>
    @else
        <p>Desde el controller</p>
    @endif
@endsection
