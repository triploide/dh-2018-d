<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Películas</title>
</head>
<body>
	<h1>Películas</h1>
	@foreach ($pelis as $peli)
		<p>{{ $peli->title }} - rating: {{ $peli->rating }}  - estreno: {{ $peli->release_date }}</p>
	@endforeach
</body>
</html>