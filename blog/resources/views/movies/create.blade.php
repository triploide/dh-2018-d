<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Crear película</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style type="text/css">
		body {
			padding: 40px;
		}
	</style>
</head>
<body>
	<h1>Crear película</h1>
	<div class="container">
		<form action="/movies" method="post">
			@csrf
			<div class="form-group">
				<label for="title">Título</label>
				<input type="text" name="title" id="title" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit">Guardar</button>
			</div>
		</form>
	</div>
</body>
</html>