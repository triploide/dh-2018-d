<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Géneros</title>
</head>
<body>
	<h1>Géneros</h1>

	<ul>
		@foreach ($generos as $genero)
			<li>{{ $genero->name }}</li>
		@endforeach
	</ul>
</body>
</html>