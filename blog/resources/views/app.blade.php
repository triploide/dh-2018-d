<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>@yield('title', 'Rent a movie')</title>
	@include('partials/head')
</head>
<body>
	<nav>
		<ol>
			<li><a href="#">Uno</a></li>
			<li><a href="#">Dos</a></li>
			<li><a href="#">Tres</a></li>
		</ol>
	</nav>

	<main>
		{{-- 
			@section('contenido')
				<h1>Rent a movie</h1>
			@show
		 --}}

		@yield('contenido')
	</main>

	<footer>
		<p>Footer</p>
	</footer>
</body>
</html>