<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'price' => rand(1000, 9999),
        'category_id' => rand(1,3)
    ];
});
