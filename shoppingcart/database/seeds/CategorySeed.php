<?php

use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /*
     * \DB::table('categories')->where() = Category::where()
     */
    public function run()
    {
        \DB::table('categories')->insert([
        	['name' => 'Motorola'],
        	['name' => 'Apple'],
        	['name' => 'Samsung']
        ]);
    }
}
