<?php

use Illuminate\Database\Seeder;

class TagSeed extends Seeder
{
    /*
     * \DB::table('categories')->where() = Category::where()
     */
    public function run()
    {
        \DB::table('tags')->insert([
        	['name' => 's'],
        	['name' => 'm'],
        	['name' => 'l']
        ]);
    }
}
