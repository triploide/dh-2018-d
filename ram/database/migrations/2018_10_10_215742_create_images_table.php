<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->smallIncrements('id'); //integer autoincrement unsigned primary key
            $table->string('src', 100)->default('no-image.jpg');  //varchar
            $table->integer('movie_id')->unsigned()->index(); //integer
            $table->text('description')->nullable();

            //$table->float('price', 6, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
