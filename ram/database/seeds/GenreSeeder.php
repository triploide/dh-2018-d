<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
        	[
        		'name' => 'Test 1',
        		'ranking' => '16',
        		'active' => 1
        	],
        	[
        		'name' => 'Test 2',
        		'ranking' => '17',
        		'active' => 1
        	],
        	[
        		'name' => 'Test 3',
        		'ranking' => '18',
        		'active' => 1
        	]
        ]);
    }
}
