<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'rating' => $faker->randomFloat(2, 1, 10),
        'awards' => $faker->numberBetween(0, 15),
        'release_date' => $faker->randomFloat(2, 1, 10),
        'length' => rand(60, 180),
        'genre_id' => rand(1, 5)
    ];
});

/*
factory(App\Genre::class, 3)->make()->map(function ($genre) {
	Movie::create([
		'title' => 'sdfsdfsdf',
		'genre_id' => $genre->id
	]);
});
*/
